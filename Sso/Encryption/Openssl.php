<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * PHP Version 5.5
 *
 * @category   Flagbit
 * @package    Flagbit_Sso
 * @author     Jörg Weller <weller@flagbit.de>
 * @author     David Paz <david.paz@flagbit.de>
 * @copyright  2015 Flagbit
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://opensource.org/licenses/osl-3.0.php
 */


/**
 * Class Flagbit_Sso_Encryption_Openssl
 *
 * @category   Flagbit
 * @package    Flagbit_Sso
 * @author     David Paz <david.paz@flagbit.de>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Flagbit_Sso_Encryption_Openssl implements Flagbit_Sso_Encryption_Interface
{


    /**
     * @see Flagbit_Sso_Encryption_Interface::encrypt
     * @param mixed $plaintext
     * @param string $publicKey
     * @return string
     * @throws Exception
     */
    function encrypt($plaintext, $publicKey){
        $pub_key = openssl_get_publickey($publicKey);
        if(!openssl_public_encrypt($plaintext, $encryptedText, $pub_key)) {
            $message = array();
            while ($msg = openssl_error_string()) {
                $message[] = $msg;
            }

            throw new DomainException(implode("\n", $message));
        }

        return(base64_encode($encryptedText));
    }


    /**
     * @see Flagbit_Sso_Encryption_Interface::decrypt
     * @param mixed $encryptedText
     * @param string $privateKey
     * @return mixed
     */
    function decrypt($encryptedText, $privateKey){
        $private_key = openssl_get_privatekey($privateKey);
        if(!openssl_private_decrypt(base64_decode($encryptedText), $decrypted, $private_key)) {
            $message = array();
            while ($msg = openssl_error_string()) {
                $message[] = $msg;
            }

            throw new DomainException(implode("\n", $message));
        }

        return $decrypted;
    }

}