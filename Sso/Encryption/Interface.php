<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * PHP Version 5.5
 *
 * @category   Flagbit
 * @package    Flagbit_Sso
 * @author     Jörg Weller <weller@flagbit.de>
 * @author     David Paz <david.paz@flagbit.de>
 * @copyright  2015 Flagbit
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://opensource.org/licenses/osl-3.0.php
 */


/**
 * Class Flagbit_Sso_Encryption_Interface
 *
 * Keys can be obtained like:#
 * <code>
 * # private key
 * openssl genrsa -out mykey.pem 2048
 * # public key
 * openssl rsa -in mykey.pem -pubout -out mykey.pub
 * </code>
 *
 * Other way to get public key having only private key is:
 * <code>
 * $keyData = openssl_pkey_get_details(openssl_get_privatekey($priv));
 * # $keyData['key'] contain te public key data.
 * </code>
 *
 * @category   Flagbit
 * @package    Flagbit_Sso
 * @author     David Paz <david.paz@flagbit.de>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
interface Flagbit_Sso_Encryption_Interface
{


    /**
     * Encrypt using public key
     *
     * @param mixed  $data
     * @param string $publicKey Key file content, not path
     * @return string
     */
    public function encrypt($data, $publicKey);


    /**
     * Decrypt using private key
     *
     * @param mixed  $data
     * @param string $privateKey Kexy file content, not path
     * @return string
     */
    public function decrypt($data, $privateKey);


}