# Flagbit SSO Library

SSL wrapper magento library to sign/encrypt information sent between Typo3 and Magento. No encryption is needed as long as we use
`https` on server. When using plain `http` encryption must be used too.

# Install

Install on Magento by using composer. Include `"flagbit/library-sso": "[version]"`, also you should configure an `extra` entry in your composer like:

    "extra": {
            "installer-paths": {
                "src/lib/{$name}": ["type:magento-library"]
            }
        },

this is used by the package [Composer Installer](https://github.com/composer/installers). After running `composer install` you should get a new 
directory `Flagbit/Sso` inside you magento `lib` folder.

# Encryption

Expected format for key is an string like specified in *openssl_get_publickey* and *openssl_get_privatekey* reference. You
can generate such keys from command line like:


    # private key
    openssl genrsa -out mykey.pem 2048
    # public key
    openssl rsa -in mykey.pem -pubout -out mykey.pub


Or from php like:


    // Generate the public key for the private key
    $key = openssl_pkey_get_details($privateKey); // $privateKey is created with openssl_pkey_new()

    // Save the public key in public.pem file
    file_put_contents('public.pem', $key['key']);

## Caution about encrypting

Care must be taken for the size of data to encrypt. According to [this thread](http://php.net/manual/en/function.openssl-public-encrypt.php#115138), it is important to not incur in errors. For example when using the library to encrypt signed data from customers, the size for the generated key that suited well was: **8192**, which is kind of bit but necessary. This mean that when generating private keys command should be something like: `openssl genrsa -out mykey.pem 8192` 